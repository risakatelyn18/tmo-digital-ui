# Code smells
1. When we enter a value, for example "javascript" and click enter or on search icon, the data related to "javascript" will be displayed. But when we delete the entered text and enter another keyword or letter in the input without clicking enter button or search icon, it is still showing previous data. It should not display previous data as we are entering new values.
2. Similarly when we delete the text and enter another keyword and click on enter button or search icon, it is showing previous data first and then displaying new data as per the entered text. - Fixed this issue.

# Improvements
3. A clear icon along with search icon would be nice as we can delete the entire text at once using clear icon. - I have added this.
4. A spinner would be nice as the data is taking time to load. - I have added this.
5. The title of the book, author name and other text related to book are not accessible using tab and are not read like the buttons "Want to read".
6. Close Button in sidenav is read as "button heading level 2". If it is read as "close button", it would give better understanding of what that button is.